export interface gasto{

    id: string;
    cantidad:string;    
    descripcion:string;    
    fechaRegistro:string;   
    tipo:string;
    comentarios:string[];
}