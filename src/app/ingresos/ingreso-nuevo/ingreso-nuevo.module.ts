import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IngresoNuevoPageRoutingModule } from './ingreso-nuevo-routing.module';

import { IngresoNuevoPage } from './ingreso-nuevo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IngresoNuevoPageRoutingModule
  ],
  declarations: [IngresoNuevoPage]
})
export class IngresoNuevoPageModule {}
