import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GastosPage } from './gastos.page';

const routes: Routes = [
  {
    path: '',
    component: GastosPage
  },
  {
    path: 'detalle-gasto',
    loadChildren: () => import('./detalle-gasto/detalle-gasto.module').then( m => m.DetalleGastoPageModule)
  },
  {
    path: 'gasto-nuevo',
    loadChildren: () => import('./gasto-nuevo/gasto-nuevo.module').then( m => m.GastoNuevoPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GastosPageRoutingModule {}
