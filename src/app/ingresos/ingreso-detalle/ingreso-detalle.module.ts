import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IngresoDetallePageRoutingModule } from './ingreso-detalle-routing.module';

import { IngresoDetallePage } from './ingreso-detalle.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IngresoDetallePageRoutingModule
  ],
  declarations: [IngresoDetallePage]
})
export class IngresoDetallePageModule {}
