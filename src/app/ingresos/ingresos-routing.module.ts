import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IngresosPage } from './ingresos.page';

const routes: Routes = [
  {
    path: '',
    component: IngresosPage
  },
  {
    path: 'ingreso-detalle',
    loadChildren: () => import('./ingreso-detalle/ingreso-detalle.module').then( m => m.IngresoDetallePageModule)
  },
  {
    path: 'ingreso-nuevo',
    loadChildren: () => import('./ingreso-nuevo/ingreso-nuevo.module').then( m => m.IngresoNuevoPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IngresosPageRoutingModule {}
