export interface ingreso{

    id: string;
    cantidad:string;    
    descripcion:string;    
    fechaRegistro:string;
    boletaNo:string;
    tipo:string;
    comentarios:string[];
}