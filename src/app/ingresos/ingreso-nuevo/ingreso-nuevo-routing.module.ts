import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IngresoNuevoPage } from './ingreso-nuevo.page';

const routes: Routes = [
  {
    path: '',
    component: IngresoNuevoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IngresoNuevoPageRoutingModule {}
