import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IngresosService } from '../ingresos.service';

@Component({
  selector: 'app-ingreso-nuevo',
  templateUrl: './ingreso-nuevo.page.html',
  styleUrls: ['./ingreso-nuevo.page.scss'],
})
export class IngresoNuevoPage implements OnInit {

  constructor(private ingresosService:IngresosService,private router:Router) { }

  ngOnInit() {
  }

  grabarnuevo(cantidad,descripcion,fecha,boleta,tipo){

    this.ingresosService.addIngreso(cantidad.value,descripcion.value,fecha.value,boleta.value,tipo.value)
    this.router.navigate(['/ingresos']);
  }

}
