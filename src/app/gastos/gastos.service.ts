import { Injectable } from '@angular/core';
import {gasto} from './gasto.model'

@Injectable({
  providedIn: 'root'
})
export class GastosService {

  private gastos: gasto[] = [

    {
      id: '1',
      cantidad: '300.00',
      descripcion: 'gasolina semana 1',
      fechaRegistro: "31/01/2021",   
      tipo: "semanal",
      comentarios: ['gasolina para ir a trabajar']
    },
    {
      id: '2',
      cantidad: '900.00',
      descripcion: 'Comida para perro',
      fechaRegistro: "15/01/2021",  
      tipo: "Mensual",
      comentarios: ['donativo albergue']
    },
    {
      id: '3',
      cantidad: '900.00',
      descripcion: 'Comida para gato',
      fechaRegistro: "15/01/2021",  
      tipo: "Mensual",
      comentarios: ['donativo albergue']
    },
    {
      id: '4',
      cantidad: '900.00',
      descripcion: 'llantas',
      fechaRegistro: "15/01/2021",  
      tipo: "Mensual",
      comentarios: ['carro 1']
    }

  ]

  constructor() { }

  getGastos() {

    return [...this.gastos]
  }

  getGasto(gastoId: string) {
    return {
      ...this.gastos.find(gasto => {
        return gasto.id === gastoId
      })

    }


  }

  addGasto(cantidad, descripcion, fechaRegistro, tipo) {
    this.gastos.push({
      id: this.gastos.length + 1 + "",
      descripcion,
      cantidad,      
      fechaRegistro,   
      tipo,
      comentarios: []
    });

  }

  deleteGasto(gastoId) {
    this.gastos = this.gastos.filter(gasto => {
      return gasto.id !== gastoId
    })



  }




}
