import { Component, OnInit } from '@angular/core';
import { gasto } from '../gasto.model';
import { GastosService } from '../gastos.service';
import { ActivatedRoute, Router } from '@angular/router'
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-detalle-gasto',
  templateUrl: './detalle-gasto.page.html',
  styleUrls: ['./detalle-gasto.page.scss'],
})
export class DetalleGastoPage implements OnInit {

  gastos: gasto

  constructor(private gastoService:GastosService,private activateRoute: ActivatedRoute,private alertctrl: AlertController, private router: Router,) { }

  ngOnInit( ) {

    this.activateRoute.paramMap.subscribe(paraMap => {

      const idReci = paraMap.get('gastoId')

      this.gastos = this.gastoService.getGasto(idReci);
      console.log(this.gastos)
    })
  }


  async eliminagasto() {
    console.log('entro a funcion eliminar')
    const alerta = await this.alertctrl.create({
     header: 'seguro desea eliminar el gasto?',
     message: 'Eliminar para confirmar',
     buttons:
       [
         {
           text: 'Cancel',
           role: 'cancel'
         },
         {
           text: 'Eliminar',
           handler: () => {
             this.gastoService.deleteGasto(this.gastos.id)
             this.router.navigate(['/gastos'])
             console.log('entro a eliminar')
           }
         }
       ]
   });
   await alerta.present();

 }


}
