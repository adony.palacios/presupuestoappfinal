import { Component, OnInit } from '@angular/core';
import { IngresosService } from './ingresos.service'
import { Router } from '@angular/router'

@Component({
  selector: 'app-ingresos',
  templateUrl: './ingresos.page.html',
  styleUrls: ['./ingresos.page.scss'],
})
export class IngresosPage implements OnInit {

  ingresos = []
  numero = 0;



  constructor(private ingresosService: IngresosService, private router: Router) { }

  ngOnInit() {
    this.ingresos = this.ingresosService.getIngresos();
    this.numero=0;
    this.calcular()    
  }


  ionViewWillEnter() {
    this.ingresos = this.ingresosService.getIngresos(); 
    this.numero=0;  
    this.calcular()
  }


  NuevoIngreso() {
    console.log('nuevo ingreso')
    this.router.navigate(['/registro-ingreso']);
  }

  toggleMenu() {
    console.log('abriendo menu')
  }

calcular(){

  for (var i = 0; i < this.ingresos.length; i++) {
    this.numero = this.numero + Number(this.ingresos[i].cantidad)  
  }

}

}
