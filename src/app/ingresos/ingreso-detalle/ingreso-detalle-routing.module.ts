import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IngresoDetallePage } from './ingreso-detalle.page';

const routes: Routes = [
  {
    path: '',
    component: IngresoDetallePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IngresoDetallePageRoutingModule {}
