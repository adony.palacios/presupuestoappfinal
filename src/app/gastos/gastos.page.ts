import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GastosService } from './gastos.service';

@Component({
  selector: 'app-gastos',
  templateUrl: './gastos.page.html',
  styleUrls: ['./gastos.page.scss'],
})
export class GastosPage implements OnInit {


  gastos = []
  numero = 0;

  constructor(private gastosService: GastosService, private router: Router) { }

  ngOnInit() {
    this.gastos = this.gastosService.getGastos();
    this.numero = 0;
    this.calcular()
  }

  ionViewWillEnter() {
    this.gastos = this.gastosService.getGastos();
    this.numero = 0;
    this.calcular()

  }

  toggleMenu() {

    console.log('abriendo menu')
  }


  NuevoGasto() {

    this.router.navigate(['/registro-gasto']);
  }

  calcular() {

    for (var i = 0; i < this.gastos.length; i++) {
      this.numero = this.numero + Number(this.gastos[i].cantidad)
    }
  }
}