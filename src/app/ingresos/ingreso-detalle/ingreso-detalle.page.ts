import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'
import { IngresosService } from '../ingresos.service';
import { ingreso } from './ingreso.model';
import { AlertController } from '@ionic/angular'

@Component({
  selector: 'app-ingreso-detalle',
  templateUrl: './ingreso-detalle.page.html',
  styleUrls: ['./ingreso-detalle.page.scss'],
})
export class IngresoDetallePage implements OnInit {

  ingresos: ingreso

  constructor(private activateRoute: ActivatedRoute, private ingresosService: IngresosService, private router: Router, private alertctrl: AlertController) { }

  ngOnInit() {
    this.activateRoute.paramMap.subscribe(paraMap => {

      const idReci = paraMap.get('ingresoId')

      this.ingresos = this.ingresosService.getingreso(idReci);
      console.log(this.ingresos)
    })
  }


  async eliminaIngreso() {
     console.log('entro a funcion eliminar')
     const alerta = await this.alertctrl.create({
      header: 'seguro desea eliminar el ingreso?',
      message: 'Eliminar para confirmar',
      buttons:
        [
          {
            text: 'Cancel',
            role: 'cancel'
          },
          {
            text: 'Eliminar',
            handler: () => {
              this.ingresosService.deleteIngreso(this.ingresos.id)
              this.router.navigate(['/ingresos'])
              console.log('entro a eliminar')
            }
          }
        ]
    });
    await alerta.present();

  }

}
