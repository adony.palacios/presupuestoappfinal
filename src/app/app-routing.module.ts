import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'ingresos',
    children: [
      {
        path: "",
        loadChildren: () => import('./ingresos/ingresos.module').then(m => m.IngresosPageModule)


      },
      {
        path: ":ingresoId",
        loadChildren: () => import('./ingresos/ingreso-detalle/ingreso-detalle.module').then(m => m.IngresoDetallePageModule)
      }
    ]
  },
  {
    path: "registro-ingreso",
    loadChildren: () => import('./ingresos/ingreso-nuevo/ingreso-nuevo.module').then(m => m.IngresoNuevoPageModule)

  },

  {
    path: 'gastos',
    children: [
      {
        path: "",
        loadChildren: () => import('./gastos/gastos.module').then(m => m.GastosPageModule)


      },
      {
        path: ":gastoId",
        loadChildren: () => import('./gastos/detalle-gasto/detalle-gasto.module').then(m => m.DetalleGastoPageModule)
      }
    ]
  },

  {
    path: "registro-gasto",
    loadChildren: () => import('./gastos/gasto-nuevo/gasto-nuevo.module').then(m => m.GastoNuevoPageModule)

  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
