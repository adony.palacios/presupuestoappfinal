import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GastoNuevoPage } from './gasto-nuevo.page';

const routes: Routes = [
  {
    path: '',
    component: GastoNuevoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GastoNuevoPageRoutingModule {}
