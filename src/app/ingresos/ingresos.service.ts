import { NodeWithI18n } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { ingreso } from './ingreso-detalle/ingreso.model'

@Injectable({
  providedIn: 'root'
})
export class IngresosService {


  private ingresos: ingreso[] = [

    {
      id: '1',
      cantidad: '5000.00',
      descripcion: 'Salario mensual',
      fechaRegistro: "31/01/2021",
      boletaNo: "123456",
      tipo: "Mensual",
      comentarios: ['compra de pañales', 'compra de leche']
    },
    {
      id: '2',
      cantidad: '900.00',
      descripcion: 'Bonificacion',
      fechaRegistro: "15/01/2021",
      boletaNo: "7894561",
      tipo: "Quincenal",
      comentarios: ['compra de llantas', 'compra de frenos']
    },
    {
      id: '3',
      cantidad: '250.00',
      descripcion: 'bonificacion Ley',
      fechaRegistro: "31/01/2021",
      boletaNo: "456987",
      tipo: "Mensual",
      comentarios: []
    }

  ]

  constructor() { }

  getIngresos() {

    return [...this.ingresos]
  }

  getingreso(ingresoId: string) {
    return {
      ...this.ingresos.find(ingreso => {
        return ingreso.id === ingresoId
      })

    }


  }

  addIngreso(cantidad, descripcion, fechaRegistro, boletaNo, tipo) {
    this.ingresos.push({
      id: this.ingresos.length + 1 + "",
      descripcion,
      cantidad,      
      fechaRegistro,
      boletaNo,
      tipo,
      comentarios: []
    });

  }

  deleteIngreso(ingresoId) {
    this.ingresos = this.ingresos.filter(ingreso => {
      return ingreso.id !== ingresoId
    })



  }






}
