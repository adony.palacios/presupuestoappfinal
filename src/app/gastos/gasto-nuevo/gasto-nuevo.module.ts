import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GastoNuevoPageRoutingModule } from './gasto-nuevo-routing.module';

import { GastoNuevoPage } from './gasto-nuevo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GastoNuevoPageRoutingModule
  ],
  declarations: [GastoNuevoPage]
})
export class GastoNuevoPageModule {}
