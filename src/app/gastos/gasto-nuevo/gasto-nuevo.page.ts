import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GastosService } from '../gastos.service';

@Component({
  selector: 'app-gasto-nuevo',
  templateUrl: './gasto-nuevo.page.html',
  styleUrls: ['./gasto-nuevo.page.scss'],
})
export class GastoNuevoPage implements OnInit {

  constructor(private gastoService:GastosService,private router:Router) { }

  ngOnInit() {
  }

  grabarnuevo(cantidad,descripcion,fecha,boleta){

    this.gastoService.addGasto(cantidad.value,descripcion.value,fecha.value,boleta.value)
    this.router.navigate(['/gastos']);
  }

}
